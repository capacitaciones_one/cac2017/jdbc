package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestJdbc {

    static int ultimoRegistro = 0;

    public static void main(String[] args) {
        System.out.println("[...] main");
        consultar();
        insertar();
        consultar();
        borrar();
        consultar();
        System.out.println("[OK] main");
    }

    static void consultar() {
        System.out.println("    [...] consultar");
        String usuario = "cine";
        String clave = "cine";
        String host = "jdbc:mysql://localhost:3306/nataliaCine";
        String querySql = "SELECT * FROM candy;";

        try {
            //Se conecta a la base de datos
            Connection con = DriverManager.getConnection(host, usuario, clave);

            //Prepara la consulta sql
            PreparedStatement pstm = con.prepareStatement(querySql);

            //Ejecuta la consulta
            ResultSet rs = pstm.executeQuery();

            //Recorre los resultados y los muestra
            while (rs.next()) {
                System.out.println(rs.getString("candy_nombr")
                        + " " + rs.getString("candy_precio"));
            }
        } catch (SQLException ex) {
            System.out.println("Alerta: " + ex.getMessage());
        } finally {

        }
        System.out.println("    [OK] consultar");
    }

    static void insertar() {
        System.out.println("    [...] insertar");
        String usuario = "cine";
        String clave = "cine";
        String host = "jdbc:mysql://localhost:3306/nataliaCine";
        String nombre = "Medianluna";
        int precio = 230;
        String querySql = "INSERT INTO candy (candy_nombr, candy_precio) VALUES(?, ?);";

        try {
            //Conectar a la base de datos
            Connection con = DriverManager.getConnection(host, usuario, clave);

            //Preparar consulta SQL. Retornar ID de registro insertado
            PreparedStatement pstm = con.prepareStatement(querySql, Statement.RETURN_GENERATED_KEYS);

            //Set de datos a ingresar
            pstm.setString(1, nombre);
            pstm.setInt(2, precio);

            //Ejecutar ocnsulta
            pstm.execute();

            //Guardamos en un objeto el ID para ser tratato por otro metodos.
            ResultSet rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                ultimoRegistro = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Alert: " + ex.getMessage());
        }
        System.out.println("[OK] insertar");
    }

    static void borrar() {
        System.out.println("[...] borrar");
        String usuario = "cine";
        String clave = "cine";
        String host = "jdbc:mysql://localhost:3306/nataliaCine";
        String querySql = "DELETE FROM candy WHERE candy_id = ?";
        Connection con =null;
        try {
            con = DriverManager.getConnection(host, usuario, clave);
            PreparedStatement pstm = con.prepareStatement(querySql);
            pstm.setInt(1, ultimoRegistro);
            pstm.execute();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        

        System.out.println("[OK] borrar");
    }
}
